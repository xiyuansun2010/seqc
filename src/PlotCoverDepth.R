
##
## plot mean and error bars
## Input:
## dat - matrix of mean
## sd - matrix of sd
## col, title = method color and title for plot
## mode = [sensitivity or specificity]
## col.replicates = color scheme for replicates
##
plot.barplot.error <- function(dat, sd, col, title, mode=c("sensitivity", "specificity"),  col.replicates){
  positions <- barplot(dat, beside=TRUE, plot=FALSE)
  if(mode == "sensitivity"){
    y.lim <- c(0.0,1.02)
    y.lab <- 'Sensitivity fraction of TF'
    tlt <- paste(title, mode, sep=' ')
  }else{
    y.lim <- c(0, 500)
    y.lab <- 'Number of FP genes'
    tlt <- paste(title, 'FP gene', sep=' ')
  }
  
  plot(positions, (dat+sd), type='n',
       ylim=y.lim,
       xaxt='n', ##supress x-axis
       xlab="Seq. Depth", ylab=y.lab, main=tlt)
  
  for(i in 1:dim(dat)[1]){ ## replicates
    for(j in 1:dim(dat)[2]){ ## coverage
      
      if(sd[i,j] >0){

        ## calculate standard error = sd/sqrt(n)
        ## where n is the number of tests
        if(j < dim(dat)[2] & i < dim(dat)[1]){ 
          se <- sd[i,j]/sqrt(choose(choose(5,i),2)*5)
          
        }else if( j< dim(dat)[2] & i== dim(dat)[1]){ ## i==5 using all replicates
          se <- sd[i,j]/sqrt(5)

        }else if(j == dim(dat)[2] & i< dim(dat)[1]){ ## j==7 using all reads, full coverage
          se <- sd[i,j]/sqrt(choose(choose(5,i),2))
        }
        
        arrows(positions[i,j], dat[i,j]+se,
               positions[i,j], dat[i,j]-se,
               code=3, col=col, angle=90, lty=i, lwd=1.75, length=0.05)
      }
      points(positions[i,j], dat[i,j], pch=19, col=col.replicates[i])
    }
  }
  axis(1, at=positions[1,], labels=colnames(dat))
  legend(ifelse(mode == 'sensitivity', 'bottomright','topleft'), legend=rownames(dat), col=col.replicates, pch=19)
}

##
## calculate se.
## input: sd matrix of this form
##           coverage
## replicates        5%       10%        20%       30%       40%        50%  100% 
##       2rep 188.24507 191.87492 193.062723 193.22397 193.54986 194.710402 76.71117
##       3rep 118.47661 120.39243 122.920722 123.17602 124.11452 124.431672 62.08111
##       4rep  49.91758  54.05026  55.654257  56.15636  54.94301  56.286733 58.85245
##       5rep  13.01153  10.69112   9.038805  11.05893  15.14265   6.730527 0
## output:
##  matrix of se with same dim as dat 
calculateSE <- function(dat){
  se <- matrix(0, nrow=dim(dat)[1], ncol=dim(dat)[2])
  for(i in 1:dim(dat)[1]){
    for(j in 1:dim(dat)[2]){
      if(dat[i,j] > 0){
        
        if(j < dim(dat)[2] & i< dim(dat)[1]){
          se[i,j] <- dat[i,j]/sqrt(choose(choose(5,i),2)*5)

        }else if( j< dim(dat)[2] & i== dim(dat)[1]){ ## i==5 using all replicates, last row
          se[i,j] <- dat[i,j]/sqrt(5)

        }else if(j == dim(dat)[2] & i< dim(dat)[1]){ ## j==7 using all reads, full coverage, last column
          se[i,j] <- dat[i,j]/sqrt(choose(choose(5,i),2))
        }
      }
    }
  }
  return(se)
          
}

##
## Convert matrices to data frames to be used by ggplot
## Input: list of mean, followed by sd where each element in the list is a matrix
## note that the order is importatn: fp, sd.fp, fp.1q, sd.fp.1q,...
## Output:
##  dataframe of replicate, coverage, mean, sd, se, expression quantile
##  this is used by ggplot
reshape.matrix2dataframe <- function(lst){
  require("reshape")
  all.data <- data.frame(stringsAsFactors=FALSE)
  ## Collect data into one datafarme
  for(i in grep("sd", names(lst), invert=T)){
    dfr <- melt(lst[[i]]) ## collect means
    dfr <- cbind(dfr, melt(lst[[i+1]])[,3]) ## collect sd

    ## standard error
    dfr <- cbind(dfr, melt(calculateSE(lst[[i+1]]))[,3])
    
    ## add expression quantile
    quantileNum <- regexpr("[0-9]", names(lst)[i])
    expression.quantile <- ifelse(quantileNum == -1,0,
        substr(names(lst)[i], start=quantileNum, stop=quantileNum+attr(quantileNum, "match.length")-1))
    dfr <- cbind(dfr,expression.quantile)
    all.data <- rbind(all.data, dfr)
  }

  colnames(all.data) <- c("Replicates", "Coverage", "mean", "sd", "se", "expression.quantile")

  return(all.data)
}

####################
## Main
####################
methods = c("deseq","edger","limmaqn","voom","poisson")
titles = c("DESeq","edgeR","limmaQN","limmaVoom","PoissonSeq")
col.methods <- c("#A6CEE3", "#1F78B4", "#B2DF8A", "#33A02C", "#FB9A99")
col.replicates <- c("#FECC5C","#FD8D3C","#E31A1C", "brown")

plot2file=TRUE
if(plot2file)
  pdf(paste("../results/CoverageDepth_", Sys.Date(), ".pdf", sep=''))

for(i in 1:length(methods)){

  ##load data file. output from draw_covereplicates
  load(paste("../data/DepthCoverage_", methods[i], "_2012-12-27.Rdata", sep=''))

  ###################
  ## Normal plotting
  ###################
  
  ## plot.barplot.error(fp,sd.fp, col.methods[i], titles[i], mode="specificity", col.replicates)
  ## plot.barplot.error(sensitivity,sd.sensitivity, col.methods[i], titles[i], mode="sensitivity", col.replicates)

  ## ## top 25% expression
  ## plot.barplot.error(fp.4q, sd.fp.4q, col.methods[i],
  ##                    paste(titles[i], "Top 25% expression", sep=' '), mode="specificity", col.replicates)
  ## plot.barplot.error(sensitivity.4q, sd.sensitivity.4q, col.methods[i],
  ##                    paste(titles[i], "Top 25% expression", sep=' '), mode="sensitivity", col.replicates)

  ## ## bottom 25% expression
  ## plot.barplot.error(fp.1q, sd.fp.1q, col.methods[i],
  ##                    paste(titles[i], "Bottom 25% expression", sep=' '), mode="specificity", col.replicates)
  ## plot.barplot.error(sensitivity.1q, sd.sensitivity.1q, col.methods[i],
  ##                    paste(titles[i], "Bottom 25% expression", sep=' '), mode="sensitivity", col.replicates)
}

if(plot2file){
  dev.off()
  ## larger pdf for ggplots
  pdf(paste("../results/CoverageDepth_ggplot_", Sys.Date(), ".pdf", sep=''), width=16, height=11)
}

## global data frames to collect data from all methods
all.methods.fp.dfr <- data.frame()
all.methods.sensitivity.dfr <- data.frame()

########################
## Using ggplot
#######################
for(i in 1:length(methods)){
  require("ggplot2")
  require("grid")
  ##load data file. output from draw_covereplicates
  load(paste("../data/DepthCoverage_", methods[i], "_2012-12-27.Rdata", sep=''))

  ## general theme
  plot.theme <- theme(strip.text.y = element_text(face='bold'),
        legend.position='bottom',
          legend.key.size = unit(2.5, "cm"),
          legend.text=element_text(size=rel(1.2)),
          legend.title=element_text(size=rel(1.2)),
          axis.title.x=element_text(face="bold", size = rel(1.2)),
          axis.title.y=element_text(face="bold", size = rel(1.2)),
          axis.text.x=element_text(size=rel(1.2)),
          axis.text.y=element_text(size=rel(1.2)))

  ## #########
  ## FP rates
  ## #########
  ## Combine all fp matrices to a data frame 
  dfr <- reshape.matrix2dataframe(list(fp=fp, sd.fp=sd.fp,
                                  fp.4q=fp.4q, sd.fp.4q=sd.fp.4q,
                                  fp.3q=fp.3q, sd.fp.3q=sd.fp.3q,
                                  fp.2q=fp.2q, sd.fp.2q=sd.fp.2q,
                                  fp.1q=fp.1q, sd.fp.1q=sd.fp.1q))
  
  ## reorder the coverage and expression quantile for proper plotting 
  dfr$Coverage <- factor(dfr$Coverage, levels=c("5%", "10%", "20%", "30%", "40%", "50%", "100%"))

  dfr$expression.quantile <- factor(dfr$expression.quantile,
      labels=c("all genes", "First count quartile", "Second count quartile", "Third count quartile", "Fourth count quartile"))

  p <- ggplot(dfr, aes(x=Coverage, y=mean, colour=Replicates)) + ## coloring by replicates
    geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=0.4, size=1.0) + 
        scale_colour_manual(values=col.replicates) +geom_point(colour=col.methods[i]) ## set replicate coloring and points by method
  
  p <- p + facet_grid(. ~ expression.quantile) + ## facet by expression quantile
    labs(title=paste("Number of false positives", titles[i], sep=' ')) ## set title for the plot
    ##guides(shape=guide_legend(override.eas = list(shape=10)), size=rel(0.1))
  print(p)
  all.methods.fp.dfr <- rbind(all.methods.fp.dfr, cbind(dfr, titles[i]))

  #####################
  ## sensitivity rates
  #####################
  dfr <- reshape.matrix2dataframe(list(sensitivity=sensitivity, sd.sensitivity=sd.sensitivity,
                                  sensitivity.4q=sensitivity.4q, sd.sensitivity.4q=sd.sensitivity.4q,
                                  sensitivity.3q=sensitivity.3q, sd.sensitivity.3q=sd.sensitivity.3q,
                                  sensitivity.2q=sensitivity.2q, sd.sensitivity.2q=sd.sensitivity.2q,
                                  sensitivity.1q=sensitivity.1q, sd.sensitivity.1q=sd.sensitivity.1q))
  
  ## reorder the coverage and expression quantile for proper plotting 
  dfr$Coverage <- factor(dfr$Coverage, levels=c("5%", "10%", "20%", "30%", "40%", "50%", "100%"))
  dfr$expression.quantile <- factor(dfr$expression.quantile,
      labels=c("all genes", "First count quartile", "Second count quartile", "Third count quartile", "Fourth count quartile"))
  
  p <- ggplot(dfr, aes(x=Coverage, y=mean, colour=Replicates)) +
    geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=0.4, size=1.0) +
        scale_colour_manual(values=col.replicates) +geom_point(colour=col.methods[i])
  
  p <- p + facet_grid(. ~ expression.quantile) + labs(title=paste("Sensitivity rates", titles[i], sep=' '))
  print(p)
  all.methods.sensitivity.dfr <- rbind(all.methods.sensitivity.dfr, cbind(dfr, titles[i]))

  
}
#############################
## FP Summary of all methods
#############################
colnames(all.methods.fp.dfr)[7] <-c("Method")

p <- ggplot(all.methods.fp.dfr, aes(x=Coverage, y=mean, colour=Replicates)) +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=0.2, size=0.5) +
  scale_colour_manual(values=col.replicates)+geom_point(size=1.5) + labs(title="Number of false positives")

p <- p + facet_grid(Method ~ expression.quantile) + ## facet by Method (rows) and expression (columns)
  plot.theme
  ## theme(strip.text.y = element_text(face='bold'), ## modify y.axis strip
  ##       legend.position='bottom') ## move legend
print(p)

######################################
## Sensitivity Summary of all methods
######################################
colnames(all.methods.sensitivity.dfr)[7] <-c("Method")
p <- ggplot(all.methods.sensitivity.dfr, aes(x=Coverage, y=mean, colour=Replicates)) +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se),  width=0.2, size=0.5) +
  scale_colour_manual(values=col.replicates)+geom_point(size=1.5)

p <- p + facet_grid(Method ~ expression.quantile) + labs(title="Sensitivity rates") +
  plot.theme
    ## theme(strip.text.y = element_text(face='bold'),
    ##     legend.position='bottom')
print(p)

if(plot2file)
  dev.off()
