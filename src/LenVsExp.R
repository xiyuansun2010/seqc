gene.length <-  read.table("../data/gene_length.txt", sep='\t', stringsAsFactors=FALSE,col.names=c('gene','length', 'pos'))
load("../data/HTSeq.RData")
counts.dat <- HTSeq.dat
counts.dat <- matrix(as.numeric(HTSeq.dat), nrow= nrow(HTSeq.dat))
rownames(counts.dat) <- rownames(HTSeq.dat)
colnames(counts.dat) <- colnames(HTSeq.dat)
counts.dat <- counts.dat[grep('no_feature|ambiguous|too_low_aQual|not_aligned|alignment_not_unique' ,
                              rownames(counts.dat), invert=TRUE),]


##normalize by RPKM
counts.dat <- apply(counts.dat, 2, function(x) x/(sum(x)/1e6))
seqlen <- unlist(sapply(rownames(counts.dat), function(x) gene.length[gene.length[,1] == x,'length'][1]))

signal = rowMeans(counts.dat)
quantiles = quantile(signal)

## plot(log(seqlen), log(signal), pch=19, col='gray')


## create quantile groups.
ind <- cut(counts.dat, quantiles, include.lowest = TRUE, labels=names(quantiles)[2:5]) 
dd <- cbind(ind, seqlen)

pdf(paste("../results/LenVsExp_",Sys.Date(), ".pdf",sep=''))
boxplot( seqlen ~ ind, data=dd, log='y', names=names(quantiles)[2:5],
        xlab='expression quartile', ylab='length')

dev.off()
